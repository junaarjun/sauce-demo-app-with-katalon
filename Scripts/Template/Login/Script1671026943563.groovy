import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startExistingApplication(GlobalVariable.appID)

Mobile.waitForElementPresent(findTestObject('Object Repository/homepage/appLogo'), 5)

Mobile.tap(findTestObject('Object Repository/homepage/btnMenu'), 0)

Mobile.waitForElementPresent(findTestObject('Object Repository/menu/menuView'), 0)

Mobile.tap(findTestObject('Object Repository/menu/loginMenu'), 0)

Mobile.waitForElementPresent(findTestObject('Object Repository/loginpage/loginPageTitle'), 0)

Mobile.setText(findTestObject('Object Repository/loginpage/usernameField'), findTestData('credentialLogin').getValue(1, 
        5), 1)

Mobile.setText(findTestObject('Object Repository/loginpage/passwordField'), findTestData('credentialLogin').getValue(2, 
        5), 1)

Mobile.tap(findTestObject('Object Repository/loginpage/btnLogin'), 3)

