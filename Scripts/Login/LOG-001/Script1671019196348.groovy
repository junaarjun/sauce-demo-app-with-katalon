import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import java.sql.Driver as Driver
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory as MobileDriverFactory
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startExistingApplication(GlobalVariable.appID)

Mobile.waitForElementPresent(findTestObject('Object Repository/homepage/appLogo'), 5)

Mobile.tap(findTestObject('Object Repository/homepage/btnMenu'), 1)

Mobile.waitForElementPresent(findTestObject('Object Repository/menu/menuView'), 1)

Mobile.tap(findTestObject('Object Repository/menu/loginMenu'), 1)

Mobile.waitForElementPresent(findTestObject('Object Repository/loginpage/loginPageTitle'), 1)

Mobile.setText(findTestObject('Object Repository/loginpage/usernameField'), username, 1)

Mobile.setText(findTestObject('Object Repository/loginpage/passwordField'), password, 1)

Mobile.tap(findTestObject('Object Repository/loginpage/btnLogin'), 3)

if (Mobile.verifyElementExist(findTestObject('Object Repository/loginpage/alertUsernameIsRequired'), 2, FailureHandling.OPTIONAL)) {
    assert true

    println('LOG-001(Negative)')
} else if (Mobile.verifyElementExist(findTestObject('Object Repository/loginpage/alertPasswordIsRequired'), 2, FailureHandling.OPTIONAL)) {
    assert true

    println('LOG-002(Negative)')
} else if (Mobile.verifyElementExist(findTestObject('Object Repository/loginpage/alertInvalidCredential'), 2, FailureHandling.OPTIONAL)) {
    assert true

    println('LOG-003 and LOG-004(Negative)')
} else if (Mobile.verifyElementExist(findTestObject('Object Repository/homepage/tvProducts'), 2, FailureHandling.OPTIONAL)) {
    assert true

    println('LOG-005(Positi)')
}

driver = MobileDriverFactory.getDriver()

driver.terminateApp(GlobalVariable.appID)

